## Contributor Code of Conduct

To paraphrase a VGR&D inspiration: be polite, be professional, because we have a plan to `killall` your future involvement if you are not.

"Polite and professional" should not be confused with nice.  Contributions -- code, issue reports, etc. -- and action on them should be technically-focused and direct.
